﻿using System.Linq;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;

namespace Hjklvfr.UltimumSpes.Alerts.Extensions
{
    public static class CheckResultExtensions
    {
        public static string ErrorToHtml(this CheckResult checkResult)
        {
            return $"<ul>{checkResult.ErrorToHtmlList()}</ul>";
        }
        
        public static string ErrorToHtmlList(this CheckResult checkResult)
        {
            return string.Join('\n', checkResult.ErrorToText().Split('\n')
                .Select(x => $"<li>{x}</li>"));
        }
        
        public static string ErrorToText(this CheckResult checkResult)
        {
            return $"WIF: {checkResult.Wif}\n" +
                   $"{checkResult.Addresses.ToText()}\n" +
                   $"Exception: {checkResult.Exception.Message}";
        }
    }
}
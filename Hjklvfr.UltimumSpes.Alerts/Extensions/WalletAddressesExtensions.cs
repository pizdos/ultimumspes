﻿using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;

namespace Hjklvfr.UltimumSpes.Alerts.Extensions
{
    public static class WalletAddressesExtensions
    {
        public static string ToText(this WalletAddresses walletAddresses)
        {
            return $"Legacy: {walletAddresses.Legacy}\n" +
                   $"SegwitP2SH: {walletAddresses.SegwitP2Sh}";
        }
    }
}
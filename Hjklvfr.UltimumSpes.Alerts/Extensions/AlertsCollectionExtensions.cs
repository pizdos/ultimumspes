﻿using Hjklvfr.UltimumSpes.Alerts.Abstractions;
using Hjklvfr.UltimumSpes.Alerts.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.UltimumSpes.Alerts.Extensions
{
    public static class AlertsCollectionExtensions
    {
        public static IServiceCollection AddAlertsService(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddScoped<IAlertService, AlertService>();
            services.Configure<AlertOptions>(configuration);
            return services;
        }
    }
}
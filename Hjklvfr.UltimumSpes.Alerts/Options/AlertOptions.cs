﻿namespace Hjklvfr.UltimumSpes.Alerts.Options
{
    public class AlertOptions
    {
        public bool Enabled { get; set; }
        public string EmailTo { get; set; }
    }
}
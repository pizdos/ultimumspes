﻿using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.Alerts.Abstractions;
using Hjklvfr.UltimumSpes.Alerts.Extensions;
using Hjklvfr.UltimumSpes.Alerts.Options;
using Hjklvfr.UltimumSpes.EmailSender.Abstractions;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Hjklvfr.UltimumSpes.Alerts
{
    public class AlertService : IAlertService
    {
        private readonly ILogger<AlertService> _logger;
        private readonly IEmailSender _emailSender;
        private readonly AlertOptions _options;

        public AlertService(
            IEmailSender emailSender,
            IOptions<AlertOptions> options,
            ILogger<AlertService> logger)
        {
            _logger = logger;
            _options = options.Value;
            if (!_options.Enabled)
                _logger.LogWarning("Alerts, send alerts disabled from configuration");
            else if (string.IsNullOrEmpty(_options.EmailTo))
            {
                _logger.LogWarning("MailKit, send email disabled, 'FromEmail' in configuration not found");
                _options.Enabled = false;
            }
            else
                _emailSender = emailSender;
        }

        public async Task SendWalletFoundAsync(Wallet wallet)
        {
            if (!_options.Enabled)
                return;
            var subject = wallet.Balance > 0 ? "Wallet with balance was found" : "Real wallet was found";
            var senderResult = await _emailSender.SendEmailAsync(
                _options.EmailTo, subject,
                wallet.ToHtml(), wallet.ToText());

            switch (senderResult.Succeeded)
            {
                case true when senderResult.Skipped:
                    _logger.LogWarning("Alert WalletFound was skipped");
                    break;
                case false:
                    _logger.LogError("Alert WalletFound not sent: {Message}", senderResult.ErrorMessage);
                    break;
            }
        }

        public async Task SendCheckFailedAsync(CheckResult checkResult)
        {
            if (!_options.Enabled)
                return;

            var subject =
                $"{checkResult.Exception.StatusCode} - CheckFailed";
            var senderResult = await _emailSender.SendEmailAsync(
                _options.EmailTo, subject,
                checkResult.ErrorToHtml(), checkResult.ErrorToText());
            
            switch (senderResult.Succeeded)
            {
                case true when senderResult.Skipped:
                    _logger.LogWarning("Alert CheckFailed was skipped");
                    break;
                case false:
                    _logger.LogError("Alert CheckFailed not sent: {Message}", senderResult.ErrorMessage);
                    break;
            }
        }
    }
}
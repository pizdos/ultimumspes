﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hjklvfr.UltimumSpes.KeyProducer
{
    public interface IPrivateKeyGenerator
    {
        Task<byte[]> GenerateNewPrivateKey();
        IAsyncEnumerable<byte[]> GeneratePrivateKeysEndlessAsync();
    }
}
﻿using System;
using Hjklvfr.UltimumSpes.EmailSender.Abstractions;
using Hjklvfr.UltimumSpes.EmailSender.MailKit.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.UltimumSpes.EmailSender.MailKit.Extensions
{
    public static class MailKitEmailSenderServiceCollectionExtensions
    {
        public static IServiceCollection AddMailKitEmailSender(this IServiceCollection services, Action<MailKitOptions> setupAction)
        {
            services.AddMailKitEmailSender();
            if (setupAction != null)
                services.Configure(setupAction);
            return services;
        }

        public static IServiceCollection AddMailKitEmailSender(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddMailKitEmailSender();
            services.Configure<MailKitOptions>(configuration);
            return services;
        }

        public static IServiceCollection AddMailKitEmailSender(
            this IServiceCollection services)
        {
            services.AddScoped<IEmailSender, MailKitEmailSender>();
            return services;
        }
    }
}
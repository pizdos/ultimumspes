﻿using Hjklvfr.UltimumSpes.EmailSender.Options;

namespace Hjklvfr.UltimumSpes.EmailSender.MailKit.Options
{
    public class MailKitOptions : EmailSenderOptions
    {
        public string SmtpAddress { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
    }
}
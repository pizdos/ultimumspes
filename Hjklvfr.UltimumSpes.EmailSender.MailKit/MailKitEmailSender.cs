﻿using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.EmailSender.Abstractions;
using Hjklvfr.UltimumSpes.EmailSender.MailKit.Options;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;

namespace Hjklvfr.UltimumSpes.EmailSender.MailKit
{
    public class MailKitEmailSender : EmailSenderBase
    {
        private readonly MailKitOptions _options;
        
        public MailKitEmailSender(
            ILogger<MailKitEmailSender> logger, 
            IOptions<MailKitOptions> optionsMailKit) 
            : base(logger, optionsMailKit)
        {
            if (!OptionsEmailSender.Enabled)
                logger.LogWarning("MailKit, send email disabled from configuration");
            else
            {
                // config validation
                if (string.IsNullOrEmpty(optionsMailKit.Value.SmtpAddress))
                {
                    logger.LogWarning("MailKit, send email disabled, 'SmtpAddress' in configuration not found");
                    OptionsEmailSender.Enabled = false;
                }
                else if (string.IsNullOrEmpty(OptionsEmailSender.FromEmail))
                {
                    logger.LogWarning("MailKit, send email disabled, 'FromEmail' in configuration not found");
                    OptionsEmailSender.Enabled = false;
                }
                else if (string.IsNullOrEmpty(optionsMailKit.Value.Password))
                {
                    logger.LogWarning("MailKit, send email disabled, 'Password' in configuration not found");
                    OptionsEmailSender.Enabled = false;
                }
                else if (optionsMailKit.Value.Port < 1 && optionsMailKit.Value.Port > 65535)
                {
                    logger.LogWarning("MailKit, send email disabled, 'Port' in configuration not found or incorrect");
                    OptionsEmailSender.Enabled = false;
                }
                else
                {
                    _options = optionsMailKit.Value;
                }
            }
        }
        

        public override async Task<SenderResult> SendEmailAsync(
            string emailTo, string subject, string htmlMessage, string textMessage)
        {
            if (!OptionsEmailSender.Enabled)
            {
                return SenderResult.Skip;
            }

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Melmac", _options.FromEmail));
            emailMessage.To.Add(new MailboxAddress("", emailTo));
            emailMessage.Subject = subject;

            var bodyBuilder = new BodyBuilder {HtmlBody = htmlMessage};

            emailMessage.Body = bodyBuilder.ToMessageBody();

            using var client = new SmtpClient();
            await client.ConnectAsync(_options.SmtpAddress, _options.Port, true);
            await client.AuthenticateAsync(_options.FromEmail, _options.Password);
            await client.SendAsync(emailMessage);

            await client.DisconnectAsync(true);
            
            return SenderResult.Success;
        }

    }
}
﻿using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.UltimumSpes.Extensions
{
    public static class BlockchainComServiceCollectionExtensions
    {
        public static IServiceCollection AddBlockchainComService(
            this IServiceCollection services)
        {
            services.AddTransient<KeyService>();
            return services;
        }
    }
}
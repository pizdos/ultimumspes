﻿using NBitcoin;

namespace Hjklvfr.UltimumSpes
{
    public class KeyService
    {
        public BitcoinAddress GetAddressByPrivateKey(string privateKey)
        {
            return Key.Parse(privateKey, Network.Main).GetWif(Network.Main).GetAddress(ScriptPubKeyType.Legacy);
        }
    }
}
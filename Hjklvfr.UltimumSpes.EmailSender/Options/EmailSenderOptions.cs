﻿namespace Hjklvfr.UltimumSpes.EmailSender.Options
{
    public class EmailSenderOptions
    {
        public bool Enabled { get; set; }
        public string FromEmail { get; set; }
    }
}
﻿using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.EmailSender.Abstractions;
using Hjklvfr.UltimumSpes.EmailSender.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Hjklvfr.UltimumSpes.EmailSender
{
    public class EmailSenderBase : IEmailSender
    {
        /// <summary>
        /// Logger.
        /// </summary>
        protected readonly ILogger<IEmailSender> Logger;
        /// <summary>
        /// Options email sender.
        /// </summary>
        protected readonly EmailSenderOptions OptionsEmailSender;

        protected EmailSenderBase(
            ILogger<EmailSenderBase> logger,
            IOptions<EmailSenderOptions> optionsMailKit)
        {
            Logger = logger;
            OptionsEmailSender = optionsMailKit.Value;
        }

        /// <summary>Send Email.</summary>
        /// <param name="emailTo">To email.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="htmlMessage">Html message.</param>
        /// <param name="textMessage">Plain text message.</param>
        public virtual Task<SenderResult> SendEmailAsync(
            string emailTo,
            string subject,
            string htmlMessage,
            string textMessage)
        {
            Logger.LogInformation("EmailSender, Skip email sending to: {EmailTo}, subject: {Subject}", 
                emailTo, subject);
            return Task.FromResult(SenderResult.Skip);
        }

    }
}
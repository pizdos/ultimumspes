﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NBitcoin;

namespace Hjklvfr.UltimumSpes.KeyProducer.Random
{
    public class RandomGenerator : IPrivateKeyGenerator
    {
        public Task<byte[]> GenerateNewPrivateKey()
        {
            return Task.FromResult(new Key().GetWif(Network.Main).PrivateKey.ToBytes());
        }

        public async IAsyncEnumerable<byte[]> GeneratePrivateKeysEndlessAsync()
        {
            for (;;) yield return await GenerateNewPrivateKey();
        }
    }
}
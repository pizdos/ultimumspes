﻿using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;

namespace Hjklvfr.UltimumSpes.Alerts.Abstractions
{
    public interface IAlertService
    {
        Task SendWalletFoundAsync(Wallet wallet);

        Task SendCheckFailedAsync(CheckResult checkResult);
    }
}
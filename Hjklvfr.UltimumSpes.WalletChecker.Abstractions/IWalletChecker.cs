﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;

namespace Hjklvfr.UltimumSpes.WalletChecker.Abstractions
{
    public interface IWalletChecker
    {
        Task<CheckResult> CheckWalletsAsync(string wif, WalletAddresses addresses);
    }
}
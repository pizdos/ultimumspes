﻿namespace Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models
{
    public class WalletAddresses
    {
        public string Legacy { get; set; }
        
        public string SegwitP2Sh { get; set; }
    }
}
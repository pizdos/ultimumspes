﻿using System.Linq;

namespace Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models
{
    public class Wallet
    {
        public string Wif { get; set; }
        
        public string Address { get; set; }
        
        public int NumberOfTransactions { get; set; }
        
        public decimal TotalReceived { get; set; }
        
        public decimal Balance { get; set; }

        public string ToHtml()
        {
            return $"<ul>{ToHtmlList()}</ul>";
        }

        private string ToHtmlList()
        {
            return string.Join('\n', ToText().Split('\n')
                .Select(x => $"<li>{x}</li>"));
        }

        public string ToText()
        {
            return $"WIF: {Wif}\n" +
                   $"Address: {Address}\n" +
                   $"Number of transactions: {NumberOfTransactions}\n" +
                   $"TotalReceived: {TotalReceived}\n" +
                   $"Balance: {Balance}";
        }
    }
}
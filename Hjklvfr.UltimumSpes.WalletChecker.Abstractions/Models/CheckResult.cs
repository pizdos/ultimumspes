﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models
{
    public class CheckResult
    {
        public CheckResult(string wif, WalletAddresses addresses, IEnumerable<Wallet> result)
        {
            Result = result;
            Wif = wif;
            Addresses = addresses;
            Succeeded = true;
        }

        public CheckResult(string wif, WalletAddresses addresses, HttpRequestException exception)
        {
            Exception = exception;
            Wif = wif;
            Addresses = addresses;
            Succeeded = false;
        }

        public readonly bool Succeeded;

        public readonly IEnumerable<Wallet> Result;

        public readonly HttpRequestException Exception;

        public readonly string Wif;

        public readonly WalletAddresses Addresses;
    }
}
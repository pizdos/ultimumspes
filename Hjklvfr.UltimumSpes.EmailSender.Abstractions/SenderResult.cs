﻿namespace Hjklvfr.UltimumSpes.EmailSender.Abstractions
{
  /// <summary>
  /// Represents the result of an sender operation.
  /// </summary>
  public class SenderResult
  {
    /// <summary>
    /// Returns an <see cref="SenderResult"/> indicating a successful senders operation.
    /// </summary>
    /// <returns>
    /// An <see cref="SenderResult"/> indicating a successful operation.
    /// </returns>
    public static SenderResult Success { get; } = new()
    {
      Succeeded = true,
      Skipped = false
    };

    /// <summary>
    /// Returns an <see cref="SenderResult"/> indicating a skipped senders operation.
    /// </summary>
    /// <returns>
    /// An <see cref="SenderResult"/> indicating a skipped operation.
    /// </returns>
    public static SenderResult Skip { get; } = new()
    {
      Succeeded = true,
      Skipped = true
    };

    /// <summary>
    /// Flag indicating whether if the operation succeeded or not.
    /// </summary>
    /// <value>
    /// True if the operation succeeded, otherwise false.
    /// </value>
    public bool Succeeded { get; protected set; }

    /// <summary>
    /// Flag indicating whether if the operation skipped or not.
    /// </summary>
    /// <value>
    /// True if the operation skipped, otherwise false.
    /// </value>
    public bool Skipped { get; protected set; }

    /// <summary>Message of error.</summary>
    public string ErrorMessage { get; protected set; }

    /// <summary>
    /// Creates a <see cref="SenderResult"/> indicating a failed sender operation, with the parameter <paramref name="message" />, if applicable.
    /// </summary>
    /// <param name="message">Message of error.</param>
    public static SenderResult Failed(string message) => new SenderResult
    {
      Skipped = false,
      Succeeded = false,
      ErrorMessage = message
    };

    /// <summary>
    /// Converts the value of the current <see cref="SenderResult"/> object to its equivalent string representation.
    /// </summary>
    /// <returns>
    /// A string representation of the current <see cref="SenderResult"/> object.
    /// </returns>
    public override string ToString()
    {
      if (Skipped)
        return "Skipped";
      return !Succeeded ? "Failed: " + ErrorMessage : "Succeeded";
    }
  }

}
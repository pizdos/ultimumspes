﻿using System.Threading.Tasks;

namespace Hjklvfr.UltimumSpes.EmailSender.Abstractions
{
    /// <summary>
    /// Interface email sender.
    /// </summary>
    public interface IEmailSender
    {
        /// <summary>Send Email.</summary>
        /// <param name="emailTo">To email.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="htmlMessage">Html message.</param>
        /// <param name="textMessage">Plain text message.</param>
        Task<SenderResult> SendEmailAsync(
            string emailTo,
            string subject,
            string htmlMessage,
            string textMessage);
    }

}
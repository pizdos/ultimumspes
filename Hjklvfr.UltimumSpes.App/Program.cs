﻿using System;
using System.IO;
using System.Threading.Tasks;
using Hjklvfr.Sueta;
using Hjklvfr.Sueta.Abstractions;
using Hjklvfr.UltimumSpes.Alerts.Extensions;
using Hjklvfr.UltimumSpes.EmailSender.MailKit.Extensions;
using Hjklvfr.UltimumSpes.KeyProducer;
using Hjklvfr.UltimumSpes.KeyProducer.Random;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace Hjklvfr.UltimumSpes.App
{
    internal static class Program
    {
        public static async Task Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            var serviceProvider = services.BuildServiceProvider();

            // entry to run app
            var app = serviceProvider.GetService<App>() ?? throw new Exception("App not found");
            await app.Run(args);
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false)
                .AddEnvironmentVariables()
                .Build();

            services.AddLogging(builder =>
            {
                builder.ClearProviders();
                builder.SetMinimumLevel(LogLevel.Information);
                builder.AddNLog(configuration);
            });

            services.Configure<AppOptions>(configuration.GetSection("App"));

            services.AddTransient<App>();
            services.AddScoped<IProxyManager, ProxyManager>();
            services.AddBlockchainComWalletChecker(options =>
                options.UseLazyLoadingProxies().UseNpgsql(configuration.GetConnectionString("DbConnection"), 
                    b => b.MigrationsAssembly("Hjklvfr.UltimumSpes.App")),
                configuration.GetSection("BackgroundSearcher"));
            services.AddSingleton<IPrivateKeyGenerator, RandomGenerator>();
            services.AddMailKitEmailSender(configuration.GetSection("MailKit"));
            services.AddAlertsService(configuration.GetSection("Alerts"));
        }
    }
}
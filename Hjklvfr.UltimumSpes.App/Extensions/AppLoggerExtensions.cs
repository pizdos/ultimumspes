﻿using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;
using Microsoft.Extensions.Logging;

namespace Hjklvfr.UltimumSpes.App.Extensions
{
    public static class AppLoggerExtensions
    {
        public static void LogWallet(this ILogger logger, Wallet wallet)
        {
            logger.LogInformation("WIF: {Wif}", wallet.Wif);
            logger.LogInformation("Address: {Address}", wallet.Address);
            if (wallet.NumberOfTransactions <= 0) return;
            logger.LogInformation("Number of transactions: {NumOfTransactions}", wallet.NumberOfTransactions);
            logger.LogInformation("Total received: {TotalReceived}", wallet.TotalReceived);
            if (wallet.Balance <= 0) return;
            logger.LogInformation("Balance: {Balance}", wallet.Balance);
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.Alerts.Abstractions;
using Hjklvfr.UltimumSpes.App.Extensions;
using Hjklvfr.UltimumSpes.KeyProducer;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions;
using Hjklvfr.UltimumSpes.WalletChecker.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NBitcoin;

namespace Hjklvfr.UltimumSpes.App
{
    public class App
    {
        private readonly AppOptions _appSettings;

        private readonly IAlertService _alertService;
        private readonly IPrivateKeyGenerator _privateKeyGenerator;
        private readonly IWalletChecker _walletChecker;
        private readonly ILogger<App> _logger;

        public App(
            IOptions<AppOptions> appSettings,
            ILogger<App> logger,
            IPrivateKeyGenerator privateKeyGenerator,
            IAlertService alertService, 
            IWalletChecker walletChecker)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _appSettings = appSettings?.Value ?? throw new ArgumentNullException(nameof(appSettings));
            _privateKeyGenerator = privateKeyGenerator;
            _alertService = alertService;
            _walletChecker = walletChecker;
        }

        public async Task Run(string[] args)
        {
            await foreach (var privateKey in _privateKeyGenerator.GeneratePrivateKeysEndlessAsync())
            {
                var wif = new Key(privateKey).GetWif(Network.Main);

                var checkResult = await _walletChecker.CheckWalletsAsync(wif.ToString(), wif.GetAddresses());

                if (!checkResult.Succeeded)
                {
                    _logger.LogError(checkResult.Exception, "Check failed");
                    await _alertService.SendCheckFailedAsync(checkResult);
                    continue;
                }
                
                foreach (var wallet in checkResult.Result)
                {
                    _logger.LogWallet(wallet);
                    if (wallet.TotalReceived > 0)
                        await _alertService.SendWalletFoundAsync(wallet);
                }
            }

            _logger.LogInformation("Finished!");

            await Task.CompletedTask;
        }
    }
}
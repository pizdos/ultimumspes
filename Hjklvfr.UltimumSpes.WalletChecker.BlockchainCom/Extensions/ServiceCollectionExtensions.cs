﻿using System;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBlockchainComWalletChecker(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> poolOptionsAction,
            IConfiguration backgroundSearcherConfiguration)
        {
            services.AddBlockchainClient(poolOptionsAction, backgroundSearcherConfiguration);
            services.AddScoped<IWalletChecker, BlockchainComWalletChecker>();
            return services;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom
{
    public class BlockchainComWalletChecker : IWalletChecker
    {
        private readonly IServiceProvider _services;

        public BlockchainComWalletChecker(IServiceProvider services)
        {
            _services = services;
        }

        public async Task<CheckResult> CheckWalletsAsync(string wif, WalletAddresses addresses)
        {
            var scope = _services.CreateScope();
            var client = scope.ServiceProvider.GetRequiredService<IBlockchainComClient>();
            try
            {
                var balanceInfo =
                    await client.GetBalanceInfoAsync(addresses);

                return new CheckResult(wif, addresses, balanceInfo.Select(addressInfo =>
                {
                    var (address, infoModel) = addressInfo;
                    return new Wallet()
                    {
                        Address = address,
                        Balance = infoModel.FinalBalance,
                        NumberOfTransactions = infoModel.NumberOfTransactions,
                        TotalReceived = infoModel.TotalReceived,
                        Wif = wif
                    };
                }));
            }
            catch (HttpRequestException ex)
            {
                return new CheckResult(wif, addresses, ex);
            }
        }
    }
}
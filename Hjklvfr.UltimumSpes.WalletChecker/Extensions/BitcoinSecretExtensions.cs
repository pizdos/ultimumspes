﻿using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;
using NBitcoin;

namespace Hjklvfr.UltimumSpes.WalletChecker.Extensions
{
    public static class BitcoinSecretExtensions
    {
        public static WalletAddresses GetAddresses(this BitcoinSecret bitcoinSecret)
        {
            return new()
            {
                Legacy = bitcoinSecret.GetAddress(ScriptPubKeyType.Legacy).ToString(),
                SegwitP2Sh = bitcoinSecret.GetAddress(ScriptPubKeyType.SegwitP2SH).ToString()
            };
        }
    }
}
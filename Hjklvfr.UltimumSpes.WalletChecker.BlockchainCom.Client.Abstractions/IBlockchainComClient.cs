﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Abstractions.Models;

namespace Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Abstractions
{
    public interface IBlockchainComClient
    {
        Task<Dictionary<string, AddressBalanceInfoModel>> GetBalanceInfoAsync(WalletAddresses addresses);
    }
}
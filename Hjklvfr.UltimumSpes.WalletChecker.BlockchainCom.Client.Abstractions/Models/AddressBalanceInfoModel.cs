﻿using Newtonsoft.Json;

namespace Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Abstractions.Models
{
    public class AddressBalanceInfoModel
    {
        [JsonConstructor]
        public AddressBalanceInfoModel(
            [JsonProperty("final_balance")]
            int finalBalance, 
            [JsonProperty("n_tx")]
            int numberOfTransactions, 
            [JsonProperty("total_received")]
            int totalReceived)
        {
            FinalBalance = decimal.Divide(finalBalance, 100_000_000);
            NumberOfTransactions = numberOfTransactions;
            TotalReceived = decimal.Divide(totalReceived, 100_000_000);
        }

        public decimal FinalBalance { get; }
        public int NumberOfTransactions { get; }
        public decimal TotalReceived { get; }
    }
}
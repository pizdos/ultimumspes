﻿using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;

namespace Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Extensions
{
    public static class WalletAddressesExtensions
    {
        public static string ToQueryList(this WalletAddresses walletAddresses)
        {
            return $"{walletAddresses.Legacy}|{walletAddresses.SegwitP2Sh}";
        }
    }
}
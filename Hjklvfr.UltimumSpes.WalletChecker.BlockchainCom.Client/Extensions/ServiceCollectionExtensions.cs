﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Hjklvfr.Sueta.Abstractions;
using Hjklvfr.Sueta.Extensions;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBlockchainClient(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> poolOptionsAction,
            IConfiguration backgroundSearcherConfiguration)
        {
            services.AddSueta(poolOptionsAction, backgroundSearcherConfiguration);
            
            services.AddHttpClient<IBlockchainComClient, BlockchainComClient>(client =>
            {
                client.BaseAddress = BlockchainComClient.BaseAddress;
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }).ConfigureHttpMessageHandlerBuilder(async builder =>
            {
                var proxy = await builder.Services.GetRequiredService<IProxyManager>().GetProxy();
                var logger = builder.Services.GetRequiredService<ILogger<BlockchainComClient>>();
                logger.LogInformation("Find new proxy {Proxy}", proxy.GetProxy(BlockchainComClient.BaseAddress));
                Debug.WriteLine(proxy.GetProxy(new Uri("https://google.com")));
                builder.PrimaryHandler = new HttpClientHandler()
                {
                    AutomaticDecompression = (DecompressionMethods.GZip | DecompressionMethods.Deflate),
                    Proxy = proxy
                };
            }).SetHandlerLifetime(TimeSpan.FromMinutes(1));
            return services;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Hjklvfr.UltimumSpes.WalletChecker.Abstractions.Models;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Abstractions;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Abstractions.Models;
using Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client.Extensions;

namespace Hjklvfr.UltimumSpes.WalletChecker.BlockchainCom.Client
{
    public class BlockchainComClient : IBlockchainComClient
    {
        private readonly HttpClient _httpClient;

        public static Uri BaseAddress { get; set; } = new("https://blockchain.info");

        public BlockchainComClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Dictionary<string, AddressBalanceInfoModel>> GetBalanceInfoAsync(WalletAddresses addresses)
        {
            var response =
                await _httpClient.GetAsync($"{BaseAddress}/balance?active={addresses.ToQueryList()}");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<Dictionary<string, AddressBalanceInfoModel>>();
        }
    }
}